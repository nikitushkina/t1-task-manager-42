package ru.t1.nikitushkina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.request.TaskShowByIndexRequest;
import ru.t1.nikitushkina.tm.dto.response.TaskShowByIndexResponse;
import ru.t1.nikitushkina.tm.dto.model.TaskDTO;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Show task by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @Nullable final TaskShowByIndexRequest request = new TaskShowByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final TaskShowByIndexResponse response = getTaskEndpoint().showTaskByIndex(request);
        @Nullable final TaskDTO task = response.getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
