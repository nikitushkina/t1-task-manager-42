package ru.t1.nikitushkina.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @Nullable
    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

}
